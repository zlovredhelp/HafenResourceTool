#include "layerwidget.h"

#include "resources/layers/layer.h"

LayerWidget::LayerWidget(QWidget *parent) :
    QWidget(parent)
{
    m_resource = 0;
    setupUi(this);
}

void LayerWidget::setResource(Resource *res)
{
    m_resource = res;
    redrawList();
}

void LayerWidget::redrawList()
{
    layersList->clear();

    // Disable buttons
    pushNewLayer->setEnabled(false);
    pushRemoveRestore->setEnabled(false);

    if (m_resource) {
        QList<Layer *> layers = m_resource->layers();
        foreach (Layer *l, layers) {
            QColor color;
            QString itemText = "";
            if (l->isEdited()) {
                color = QColor(46, 125, 50);
                itemText = "*";
            } else if (l->isRemoved()) {
                color = QColor(198, 40, 40);
                itemText = "-";
            } else {
                color = Qt::black;
            }
            QListWidgetItem *item = new QListWidgetItem(itemText + l->type(), layersList);
            item->setTextColor(color);
        }
    }
}

void LayerWidget::on_layersList_activated(const QModelIndex &index)
{
    int row = index.row();
    emit itemActivated(row);
}

void LayerWidget::on_layersList_clicked(const QModelIndex &index)
{
    int row = index.row();
    if (m_resource && m_resource->layers().length() - 1 >= row) {
        Layer *l = m_resource->layers().at(row);
        pushRemoveRestore->setText(l->isRemoved() ? "Restore" : "Remove");
        pushRemoveRestore->setEnabled(true);
    }
}

void LayerWidget::on_pushRemoveRestore_clicked()
{
    int row = layersList->currentRow();
    if (m_resource && m_resource->layers().length() - 1 >= row) {
        Layer *l = m_resource->layers().at(row);
        l->setRemoved(!l->isRemoved());
        redrawList();

        emit resourceChanged();
    }
}
