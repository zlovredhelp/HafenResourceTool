#include "bonelistfield.h"
#include "../helpers/point3fdialog.h"
#include <QInputDialog>

BoneListField::BoneListField(const QString &title, const QMap<QString, SkelLayer::Bone> &value, QWidget *parent) :
    Field(title, parent)
{
    setupUi(this);
    label->setText(QString("%1:").arg(title));
    m_bones = value;
    redrawList();
}

void BoneListField::redrawList()
{
    treeWidget->clear();

    QMap<QString, SkelLayer::Bone>::iterator it;
    for (it = m_bones.begin(); it != m_bones.end(); ++it) {
        SkelLayer::Bone b = it.value();
        QTreeWidgetItem *bi = new QTreeWidgetItem(QStringList() << it.key() << QString());
        treeWidget->addTopLevelItem(bi);
        bi->addChild(new QTreeWidgetItem(QStringList() << "pos" << b.pos.toString()));
        bi->addChild(new QTreeWidgetItem(QStringList() << "rax" << b.rax.toString()));
        bi->addChild(new QTreeWidgetItem(QStringList() << "rang" << QString("%1").arg(b.rang)));
        bi->addChild(new QTreeWidgetItem(QStringList() << "parent" << b.bp));
    }
}

bool BoneListField::editPoint3f(const QString &parent, const QString &type)
{
    if (parent.isEmpty() || type.isEmpty() || !m_bones.contains(parent))
        return false;

    SkelLayer::Bone b = m_bones.value(parent);
    Point3F p;
    if (type == "pos")
        p = b.pos;
    else if (type == "rax")
        p = b.rax;
    else
        return false;

    Point3fDialog p3fd(p, this);
    if (!p3fd.exec())
        return false;

    p = p3fd.getValue();
    if (type == "pos")
        m_bones[parent].pos = p;
    else if (type == "rax")
        m_bones[parent].rax = p;
    return true;
}

bool BoneListField::editParent(const QString &parent, const QString &current)
{
    if (parent.isEmpty() || current.isEmpty() || !m_bones.contains(parent))
        return false;

    QList<QString> k = m_bones.keys();
    k.removeOne(parent);
    k.prepend(QString());
    int index = k.indexOf(current);
    bool ok;
    QString result = QInputDialog::getItem(this, "New value", "Parent:", k, index, false, &ok);
    if (!ok || result == current)
        return false;

    m_bones[parent].bp = result;
    return true;
}

bool BoneListField::editRang(const QString &parent)
{
    if (parent.isEmpty() || !m_bones.contains(parent))
        return false;

    float rang = m_bones[parent].rang;
    bool ok;
    float nr = (float)QInputDialog::getDouble(this, "New value", "Rang:", (double)rang, -9999, 9999, 6, &ok);
    if (!ok || nr == rang)
        return false;

    m_bones[parent].rang = nr;
    return true;
}

void BoneListField::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column)
    if (!item->parent())
        return;

    QString parentName = item->parent()->text(0);
    QString itemName = item->text(0);

    bool r = false;
    if (itemName == "pos" || itemName == "rax")
        r = editPoint3f(parentName, itemName);
    else if (itemName == "parent")
        r = editParent(parentName, itemName);
    else if (itemName == "rang")
        r = editRang(parentName);
    else
        return;

    if (r) {
        onFieldChanged();
        redrawList();
    }
}
