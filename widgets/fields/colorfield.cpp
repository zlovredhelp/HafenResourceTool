#include "colorfield.h"
#include <QColorDialog>
#include <QPalette>

ColorField::ColorField(const QString &title, const QColor &value, QWidget *parent) :
    Field(title, parent)
{
    setupUi(this);
    m_value = value;
    label->setText(QString("%1:").arg(title));
    setColorLabelText(value);
    setWidgetBgColor(value);
}

void ColorField::on_pushButton_clicked()
{
    QColor c = QColorDialog::getColor(m_value, this, "New color");
    if (!c.isValid())
        return;

    m_value = c;
    setColorLabelText(c);
    setWidgetBgColor(c);
    onFieldChanged();
}

void ColorField::setColorLabelText(const QColor &value)
{
    labelColor->setText(QString("RGBA(%1, %2, %3, %4)")
                        .arg(value.red()).arg(value.green()).arg(value.blue()).arg(value.alpha()));
}

void ColorField::setWidgetBgColor(const QColor &value)
{
    QPalette p = widget->palette();
    p.setColor(QPalette::Background, value);
    widget->setPalette(p);
    widget->setAutoFillBackground(true);
    widget->update();
}
