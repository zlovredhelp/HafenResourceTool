#ifndef MODELPREVIEW_H
#define MODELPREVIEW_H

#include <Qt3DCore/QEntity>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QCameraLens>
#include <Qt3DRender/QMesh>
#include <Qt3DRender/QTextureImage>
#include <Qt3DRender/QDirectionalLight>
#include <Qt3DRender/QRenderAspect>
#include <Qt3DCore/QTransform>
#include <Qt3DCore/QAspectEngine>
#include <Qt3DInput/QInputAspect>
#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QNormalDiffuseSpecularMapMaterial>
#include <Qt3DExtras/Qt3DWindow>

#include <QEvent>
#include <QCloseEvent>

class ModelPreview : public Qt3DExtras::Qt3DWindow
{
public:
    ModelPreview(const QUrl &meshUrl, const QUrl &texUrl);
    ~ModelPreview();

private:
    Qt3DCore::QEntity *m_rootEntity;
    float m_xOffset;

protected:
    bool event(QEvent *e);
};

#endif // MODELPREVIEW_H
