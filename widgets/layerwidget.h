#ifndef LAYERWIDGET_H
#define LAYERWIDGET_H

#include "resources/resource.h"

#include "ui_layerwidget.h"

class LayerWidget : public QWidget, private Ui::LayerWidget
{
    Q_OBJECT

public:
    explicit LayerWidget(QWidget *parent = 0);
    void setResource(Resource *res);
    void redrawList();

signals:
    void itemActivated(int index);
    void resourceChanged();

private slots:
    void on_layersList_activated(const QModelIndex &index);
    void on_layersList_clicked(const QModelIndex &index);
    void on_pushRemoveRestore_clicked();

private:
    Resource *m_resource;

};

#endif // LAYERWIDGET_H
