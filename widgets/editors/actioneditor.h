#ifndef ACTIONEDITOR_H
#define ACTIONEDITOR_H

#include "../editor.h"

class ActionEditor : public Editor
{
public:
    ActionEditor(Layer *l, QWidget *parent = 0);
};

#endif // ACTIONEDITOR_H
