#ifndef NEGEDITOR_H
#define NEGEDITOR_H

#include "../editor.h"

class NegEditor : public Editor
{
public:
    NegEditor(Layer *l, QWidget *parent = 0);
};

#endif // NEGEDITOR_H
