#ifndef PAGINAEDITOR_H
#define PAGINAEDITOR_H

#include "../editor.h"

class PaginaEditor : public Editor
{
public:
    PaginaEditor(Layer *l, QWidget *parent = 0);
};

#endif // PAGINAEDITOR_H
