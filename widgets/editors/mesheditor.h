#ifndef MESHEDITOR_H
#define MESHEDITOR_H

#include "../editor.h"

class MeshEditor : public Editor
{
public:
    MeshEditor(Layer *l, QWidget *parent = 0);
};

#endif // MESHEDITOR_H
