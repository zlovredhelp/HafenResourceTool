#ifndef CODEENTRYEDITOR_H
#define CODEENTRYEDITOR_H

#include "../editor.h"

class CodeentryEditor : public Editor
{
public:
    CodeentryEditor(Layer *l, QWidget *parent = 0);
};

#endif // CODEENTRYEDITOR_H
