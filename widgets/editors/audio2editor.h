#ifndef AUDIO2EDITOR_H
#define AUDIO2EDITOR_H

#include "../editor.h"

class Audio2Editor : public Editor
{
    Q_OBJECT

public:
    Audio2Editor(Layer *l, QWidget *parent = 0);

private slots:
    void saveClicked();
    void loadClicked();
};

#endif // AUDIO2EDITOR_H
