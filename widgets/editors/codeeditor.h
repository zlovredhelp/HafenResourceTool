#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include "../editor.h"

class CodeEditor : public Editor
{
public:
    CodeEditor(Layer *l, QWidget *parent = 0);
};

#endif // CODEEDITOR_H
