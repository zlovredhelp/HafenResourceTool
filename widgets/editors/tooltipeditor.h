#ifndef TOOLTIPEDITOR_H
#define TOOLTIPEDITOR_H

#include "../editor.h"

class TooltipEditor : public Editor
{
public:
    TooltipEditor(Layer *l, QWidget *parent = 0);
};

#endif // TOOLTIPEDITOR_H
