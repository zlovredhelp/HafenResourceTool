#include "hafenrestool.h"
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include "widgets/helpers/remoteresourcedialog.h"

#include "resources/layers/unknownlayer.h"

#include "widgets/editors/actioneditor.h"
#include "widgets/editors/audio2editor.h"
#include "widgets/editors/clambeditor.h"
#include "widgets/editors/codeeditor.h"
#include "widgets/editors/codeentryeditor.h"
#include "widgets/editors/defaulteditor.h"
#include "widgets/editors/foodeveditor.h"
#include "widgets/editors/imageeditor.h"
#include "widgets/editors/mat2editor.h"
#include "widgets/editors/mesheditor.h"
#include "widgets/editors/negeditor.h"
#include "widgets/editors/paginaeditor.h"
#include "widgets/editors/rlinkeditor.h"
#include "widgets/editors/skeleditor.h"
#include "widgets/editors/tileset2editor.h"
#include "widgets/editors/texeditor.h"
#include "widgets/editors/tooltipeditor.h"
#include "widgets/editors/vbuf2editor.h"

HafenResTool::HafenResTool(QWidget *parent) :
    QMainWindow(parent)
{
    m_currentResource = 0;
    m_currentEditor = 0;
    m_progress = 0;
    m_dictionary = new ResourceDictionary(this);
    setupUi(this);

    connect(layersListWidget, SIGNAL(itemActivated(int)), SLOT(onResourceLayerSelected(int)));
    connect(layersListWidget, SIGNAL(resourceChanged()), SLOT(onResourceChanged()));
}

void HafenResTool::closeEvent(QCloseEvent *event)
{
    event->ignore();
    on_actionQuit_triggered();
}

void HafenResTool::destroyCurrentEditor()
{
    if (m_currentEditor) {
        // Disconnect all signals from editor
        m_currentEditor->disconnect();
        delete m_currentEditor;
        m_currentEditor = 0;
    }
}

void HafenResTool::setNewResource(const QString &n)
{
    if (m_currentResource) {
        // Labels
        labelResourceName->setText(QString("Resource:"));
        labelResourceVersion->setText(QString("Version:"));
        actionSaveResource->setEnabled(false);
        // Disconnect signals
        m_currentResource->disconnect();
        delete m_currentResource;
        layersListWidget->setResource(0);
    }
    destroyCurrentEditor();

    m_currentResource = new Resource(n);
    connect(m_currentResource, SIGNAL(resourceInited()), SLOT(onResourceInited()));
    connect(m_currentResource, SIGNAL(resourceError(QString)), SLOT(onResourceError(QString)));
    connect(m_currentResource, SIGNAL(resourceStartDownload()), SLOT(onResourceStartDownload()));
    connect(m_currentResource, SIGNAL(resourceEndDownload()), SLOT(onResourceEndDownload()));
    m_currentResource->init();
}

void HafenResTool::saveResource()
{
    QString name = QFileDialog::getSaveFileName(this, "Save resource", "./saved", "Hafen resources (*.res)");
    if (!name.isEmpty() && m_currentResource) {
        QFile tmpFile(name);
        if (tmpFile.open(QIODevice::WriteOnly)) {
            QByteArray resData = m_currentResource->toByteArray();
            tmpFile.write(resData);
            tmpFile.close();

            if (m_currentResource->isModified()) {
                m_currentResource->makeUnmodified();
                layersListWidget->redrawList();
                labelResourceName->setText(QString("Resource: %1").arg(m_currentResource->getName()));
            }
        }
    }
}

void HafenResTool::onResourceChanged()
{
    if (m_currentResource && !m_currentResource->isModified()) {
        // TODO: change title
    }
    // Update layers list widget
    layersListWidget->redrawList();
}

void HafenResTool::onResourceError(const QString &e)
{
    QMessageBox::warning(this, windowTitle(),
                         QString("An error occured while working with resource.\n%1").arg(e));
}

void HafenResTool::onResourceInited()
{
    labelResourceName->setText(QString("Resource: %1").arg(m_currentResource->getName()));
    labelResourceVersion->setText(QString("Version: %1").arg(m_currentResource->getVersion()));
    layersListWidget->setResource(m_currentResource);
    actionSaveResource->setEnabled(true);
    destroyCurrentEditor();
    // Add to dictionary if remote
    if (!m_currentResource->isLocal())
        m_dictionary->addPath(m_currentResource->getName());
}

void HafenResTool::onResourceLayerSelected(int index)
{
    if (!m_currentResource || m_currentResource->layers().length() - 1 < index || index < 0)
        return;

    destroyCurrentEditor();
    Layer *l = m_currentResource->layers().at(index);

    // Create editor by given type
    QString type = l->type();

    UnknownLayer *ul = dynamic_cast<UnknownLayer *>(l);
    if (ul) {
        // Default editor for all unknown layers
        m_currentEditor = new DefaultEditor(l, this);
    } else {
        if (type == "action") {
            m_currentEditor = new ActionEditor(l, this);
        } else if (type == "audio2") {
            m_currentEditor = new Audio2Editor(l, this);
        } else if (type == "clamb") {
            m_currentEditor = new ClambEditor(l, this);
        } else if (type == "code") {
            m_currentEditor = new CodeEditor(l, this);
        } else if (type == "codeentry") {
            m_currentEditor = new CodeentryEditor(l, this);
        } else if (type == "foodev") {
            m_currentEditor = new FoodevEditor(l, this);
        } else if (type == "image") {
            m_currentEditor = new ImageEditor(l, this);
        } else if (type == "mat2") {
            m_currentEditor = new Mat2Editor(l, this);
        } else if (type == "mesh") {
            m_currentEditor = new MeshEditor(l, this);
        } else if (type == "neg") {
            m_currentEditor = new NegEditor(l, this);
        } else if (type == "pagina") {
            m_currentEditor = new PaginaEditor(l, this);
        } else if (type == "rlink") {
            m_currentEditor = new RlinkEditor(l, this);
        } else if (type == "skel") {
            m_currentEditor = new SkelEditor(l, this);
        } else if (type == "tileset2") {
            m_currentEditor = new Tileset2Editor(l, this);
        } else if (type == "tex") {
            m_currentEditor = new TexEditor(l, this);
        } else if (type == "tooltip") {
            m_currentEditor = new TooltipEditor(l, this);
        } else if (type == "vbuf2") {
        m_currentEditor = new Vbuf2Editor(l, this);
        } else {
            // If editor not implemented
            m_currentEditor = new DefaultEditor(l, this);
        }
    }

    if (!m_currentEditor)
        m_currentEditor = new DefaultEditor(l, this);

    // A bit of layout magic here
    editorLayoutHolder->insertWidget(0, m_currentEditor);
    editorLayoutHolder->setStretch(0, 0);
    editorLayoutHolder->setStretch(1, 1);
    connect(m_currentEditor, SIGNAL(resourceChanged()), SLOT(onResourceChanged()));
}

void HafenResTool::onResourceStartDownload()
{
    if (m_progress) return;
    m_progress = new QProgressDialog(this, Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    m_progress->setCancelButton(0);
    m_progress->setMinimum(0);
    m_progress->setMaximum(0);
    m_progress->setLabel(new QLabel("Downloading resource from server..."));
    m_progress->show();
}

void HafenResTool::onResourceEndDownload()
{
    if (m_progress) {
        delete m_progress;
        m_progress = 0;
    }
}

void HafenResTool::on_actionRemoteResource_triggered()
{
    RemoteResourceDialog rrd(m_dictionary->getList(), this);
    if (rrd.exec()) {
        QString name = rrd.getPath();
        if (!name.isEmpty())
            setNewResource(QString("http://game.havenandhearth.com/hres/%1.res").arg(name));
    }
}

void HafenResTool::on_actionLocalResource_triggered()
{
    QString name = QFileDialog::getOpenFileName(this, "Local resource", QDir::currentPath(),
                                                "Hafen resources (*.res *.cached)");
    if (!name.isEmpty())
        setNewResource(name);
}

void HafenResTool::on_actionQuit_triggered()
{
    if (m_currentResource && m_currentResource->isModified()) {
        QMessageBox::StandardButton mbr = QMessageBox::question(this, "HafenResTool",
                              "Current resource has been modified.\nDo you want save changes before exit?",
                              QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::Yes);
        if (mbr == QMessageBox::Yes)
            saveResource();
        else if (mbr == QMessageBox::Cancel)
            return;
    }
    qApp->quit();
}

void HafenResTool::on_actionSaveResource_triggered()
{
    saveResource();
}
