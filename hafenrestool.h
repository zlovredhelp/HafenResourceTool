#ifndef HAFENRESTOOL_H
#define HAFENRESTOOL_H

#include "ui_hafenrestool.h"
#include "resources/resource.h"
#include "tools/resourcedictionary.h"
#include "widgets/editor.h"

#include <QCloseEvent>
#include <QProgressDialog>

class HafenResTool : public QMainWindow, private Ui::HafenResTool
{
    Q_OBJECT

public:
    explicit HafenResTool(QWidget *parent = 0);

protected:
    void closeEvent(QCloseEvent *event);

private:
    Resource *m_currentResource;
    ResourceDictionary *m_dictionary;
    Editor *m_currentEditor;
    QProgressDialog *m_progress;

    void destroyCurrentEditor();
    void setNewResource(const QString &n);
    void saveResource();

private slots:
    void onResourceChanged();
    void onResourceError(const QString &e);
    void onResourceInited();
    void onResourceLayerSelected(int index);
    void onResourceStartDownload();
    void onResourceEndDownload();
    void on_actionRemoteResource_triggered();
    void on_actionLocalResource_triggered();
    void on_actionQuit_triggered();
    void on_actionSaveResource_triggered();
};

#endif // HAFENRESTOOL_H
