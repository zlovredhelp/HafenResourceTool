#ifndef VBUF2LAYER_H
#define VBUF2LAYER_H

#include "layer.h"
#include "meshlayer.h"
#include "texlayer.h"
#include <QPixmap>

class Vbuf2Layer : public Layer
{
public:
    struct Bones {
        QMap<QString, QList<float> > weightmap;
        QMap<QString, int> vns;
        int size;
    };

    Vbuf2Layer(const QByteArray &d, Resource *r = 0);

    virtual const QString type() const;
    virtual const QByteArray toByteArray();
    virtual bool parse();
    virtual bool init();

    const QList<float> sublayer(const QString &s) const;
    int dimension(const QString &d) const;
    const QList<QString> sublayers() const;
    const QList<MeshLayer *> meshes() const;
    const QList<TexLayer *> textures() const;
    const QString resName() const;

    void setSublayerList(const QString &s, const QList<float> &l);

private:
    int m_fl;
    int m_num;
    QMap<QString, QList<float> > m_sublayers;
    Bones m_bones;

    //
    QMap<QString, int> m_dimensions;
    QList<MeshLayer *> m_meshes;
    QList<TexLayer *> m_textures;
    void fillBones(ReadStream &s);
};

#endif // VBUF2LAYER_H
