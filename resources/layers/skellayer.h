#ifndef SKELLAYER_H
#define SKELLAYER_H

#include "layer.h"
#include "../types/point3f.h"

class SkelLayer : public Layer
{
public:
    struct Bone {
        Point3F pos;
        Point3F rax;
        float rang;
        QString bp;
    };

    SkelLayer(const QByteArray &d, Resource *r = 0);

    virtual const QString type() const;
    virtual const QByteArray toByteArray();
    virtual bool parse();

    const QMap<QString, Bone> bones() const {return m_bones;}
    void setBones(const QMap<QString, Bone> &b) {m_bones = b;}

private:
    QMap<QString, Bone> m_bones;

};

#endif // SKELLAYER_H
