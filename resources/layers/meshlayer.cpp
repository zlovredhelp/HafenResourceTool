#include "meshlayer.h"

MeshLayer::MeshLayer(const QByteArray &d, Resource *r)
    : Layer(d, r)
{

}

const QString MeshLayer::type() const
{
    return "mesh";
}

const QByteArray MeshLayer::toByteArray()
{
    QByteArray ret;
    if (m_fl & ~15)
        return ret;
    WriteStream ws(&ret);
    ws.writeChar((unsigned char)m_fl);
    ws.writeShort((unsigned short)m_num);
    ws.writeShort(m_matid);
    if (m_fl & 2)
        ws.writeShort(m_id);
    if (m_fl & 4)
        ws.writeShort(m_ref);
    if (m_fl & 8) {
        foreach (const QString &s, m_rdat)
            ws.writeString(s);
        // Write terminating 0
        ws.writeChar(0);
    }
    foreach (int p, m_ind)
        ws.writeShort(p);
    return ret;
}

bool MeshLayer::parse()
{
    if (m_rawData.isEmpty()) {
        m_error = "Resource data is empty";
        return false;
    }

    ReadStream s(&m_rawData);
    try {
        m_fl = (unsigned char)s.getChar();
        m_num = (unsigned short)s.getShort();
        m_matid = s.getShort();

        if (m_fl & 2)
            m_id = s.getShort();
        else
            m_id = -1;

        if (m_fl & 4)
            m_ref = s.getShort();
        else
            m_ref = -1;

        if (m_fl & 8) {
            while (true) {
                QString k = s.getString();
                if (!k.length()) break;
                m_rdat << k;
            }
        }

        if (m_fl & ~15) {
            m_error = QString("Unsupported flags in fastmesh: %1").arg(m_fl);
            return false;
        }
        for (int i = 0; i < m_num * 3; ++i)
            m_ind << s.getShort();
    } catch (const runtime_error &e) {
        m_error = QString(e.what());
        return false;
    }
    return true;
}
