#include "imagelayer.h"

ImageLayer::ImageLayer(const QByteArray &d, Resource *r)
    : Layer(d, r)
{

}

const QString ImageLayer::type() const
{
    return "image";
}

const QByteArray ImageLayer::toByteArray()
{
    QByteArray ret;
    WriteStream s(&ret);
    s.writeShort(m_z);
    s.writeShort(m_subz);
    s.writeChar(m_nooff ? 2 : 0);
    s.writeShort(m_id);
    s.writeCoord16(m_o);
    s.writeImage(m_image);
    return ret;
}

bool ImageLayer::parse()
{
    if (m_rawData.isEmpty()) {
        m_error = "Resource data is empty";
        return false;
    }

    ReadStream s(&m_rawData);
    try {
        m_z = s.getShort();
        m_subz = s.getShort();
        char flags = s.getChar();
        m_nooff = (flags & 2) != 0;
        m_id = s.getShort();
        m_o = s.getCoord16();
        QByteArray imageData = s.getBytes();
        if (!m_image.loadFromData(imageData)) {
            m_error = "Can't load image from resource";
            return false;
        }
    } catch (const runtime_error &e) {
        m_error = QString(e.what());
        return false;
    }

    return true;
}
