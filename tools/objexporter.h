#ifndef OBJEXPORTER_H
#define OBJEXPORTER_H

#include "resources/layers/vbuf2layer.h"
#include <QVector3D>

class ObjExporter
{
public:
    ObjExporter(Vbuf2Layer* layer);

    bool saveAs(const QString& file, const QList<MeshLayer*>& meshes);

private:
    Vbuf2Layer* m_layer;

    const QVector3D rotateVector(const QVector3D& v);
    const QString vectorToString(const QVector3D& v);
    void writeSublayer(
        const QString& prefix, int dimension, const QList<float>& dots, QTextStream& stream, bool reverse = false);
};

#endif // OBJEXPORTER_H
