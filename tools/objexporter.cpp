#include "objexporter.h"
#include <QFile>
#include <QTextStream>

#define M_PI 3.14159

#include "resources/layers/meshlayer.h"

ObjExporter::ObjExporter(Vbuf2Layer* layer)
{
    m_layer = layer;
}

bool ObjExporter::saveAs(const QString& file, const QList<MeshLayer*>& meshes)
{
    if (file.isEmpty() || meshes.isEmpty())
        return false;

    QFile expFile(file);
    if (!expFile.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream stream(&expFile);
    stream.setRealNumberNotation(QTextStream::FixedNotation);
    stream.setRealNumberPrecision(6);

    // Writing vertexes
    writeSublayer("v", m_layer->dimension("pos"), m_layer->sublayer("pos"), stream);

    // Writing normales
    writeSublayer("vn", m_layer->dimension("nrm"), m_layer->sublayer("nrm"), stream);

    // Writing skinning
    bool hasOtex = m_layer->sublayers().contains("otex");
    writeSublayer(
        "vt", m_layer->dimension(hasOtex ? "otex" : "tex"), m_layer->sublayer(hasOtex ? "otex" : "tex"), stream, true);

    // Writing faces by given ids
    for (int i = 0; i < meshes.length(); ++i)
    {
        MeshLayer* mesh = meshes.at(i);

        for (int j = 0; j < mesh->ind().length(); j += 3)
        {
            QString str = QString("f %1/%1/%1 %2/%2/%2 %3/%3/%3\n")
                              .arg(mesh->ind()[j] + 1)
                              .arg(mesh->ind()[j + 1] + 1)
                              .arg(mesh->ind()[j + 2] + 1);
            stream << str;
        }
    }

    stream.flush();
    expFile.close();
    return true;
}

const QVector3D ObjExporter::rotateVector(const QVector3D& v)
{
    QVector3D ret = v;
    ret.setY(v.y() * static_cast<float>(cos(-M_PI / 2)) - v.z() * static_cast<float>(sin(-M_PI / 2)));
    ret.setZ(v.y() * static_cast<float>(sin(-M_PI / 2)) + v.z() * static_cast<float>(cos(-M_PI / 2)));
    return ret;
}

const QString ObjExporter::vectorToString(const QVector3D& v)
{
    QString ret;
    QTextStream s(&ret);
    s.setRealNumberNotation(QTextStream::FixedNotation);
    s.setRealNumberPrecision(6);
    s << " " << v.x() << " " << v.y() << " " << v.z();
    return ret;
}

void ObjExporter::writeSublayer(
    const QString& prefix, int dimension, const QList<float>& dots, QTextStream& stream, bool reverse)
{
    if (prefix.isEmpty() || dots.isEmpty())
        return;

    // Really weird shit goes here
    // for example vectors and normales shold be turned -pi/2
    // textures are also should be turned upside down
    // is it loftar's mistake or OGRE shit?
    // Also: I don't know what should be done for (bi)tangets, anyway I don't know how to unload them
    for (int i = 0; i < dots.length(); i += dimension)
    {
        stream << prefix;
        if (prefix == "v" || prefix == "vn")
        {
            QVector3D v(dots[i], dots[i + 1], dots[i + 2]);
            v = rotateVector(v);
            stream << vectorToString(v);
        }
        else
        {
            for (int j = 0; j < dimension; ++j)
            {
                float dot = (reverse && j ? 1.0f - dots[i + j] : dots[i + j]);
                stream << " " << dot;
            }
        }
        stream << "\n";
    }
}
